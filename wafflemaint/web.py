import os
import re
import socket
import subprocess
import sys
import time

from . import TOOLS
from .util import reload_unit, check_domain_sanity

WAFFLE_IP4 = "205.152.206.250"
WAFFLE_IP6 = "2607:fcd0:100:ae00::2"

def check_dns(name):
    "Checks if a domain points to waffle.tech and bails if not"
    try:
        r = socket.getaddrinfo(name, 80)[0][4][0]
    except:
        print("{} does not exist. Aborting.".format(name))
        sys.exit(5)
    if r != WAFFLE_IP4 and r != WAFFLE_IP6:
        print("{} does not currently point at {} or {}. Aborting."
              .format(name, WAFFLE_IP4, WAFFLE_IP6))
        sys.exit(5)


def sub_web(args, env, user):
    check_domain_sanity(args.domain)
    config_path = "/etc/nginx/sites-enabled/{}.conf".format(args.domain)
    if not args.remove:
        # Check and see if DNS is correct before we attempt LE validation
        check_dns(args.domain)
        if not args.no_www:
            check_dns("www." + args.domain)

        # Do an initial nginx config, just far enough to do the LE proof.
        print("I'm going to attempt to get a TLS certificate for this domain. Wait...")
        if os.path.exists(config_path):
            print("This domain is already configured.")
            sys.exit(5)
        with open(config_path, "w") as file:
            template = env.get_template("nginx.conf.tpl")
            tvars = {"user": user, "domain": args.domain, "tls": False}
            file.write(template.render(tvars))
        reload_unit("nginx")
        time.sleep(2)

        # Now we actually talk to LE
        try:
            if args.no_www:
                ret = subprocess.check_call(["/usr/bin/certbot", "certonly", "-n",
                    "-d", args.domain,
                    "--webroot", "--webroot-path", "/var/www/well-known" ])
            else:
                ret = subprocess.check_call(["/usr/bin/certbot", "certonly", "-n",
                    "-d", args.domain, "-d", "www." + args.domain,
                    "--webroot", "--webroot-path", "/var/www/well-known" ])
        except:
            print("Something went wrong when requesting certificates. See above.")
            os.remove(config_path)
            reload_unit("nginx")
            sys.exit(5)

        # Now we check if LE actually worked
        if not os.path.exists("/etc/letsencrypt/live/{}/fullchain.pem".format(args.domain)):
            print("Obtaining cert failed, see above for reason.")
            print("You may need to wait for DNS TTL to elapse and try again.")
            os.remove(config_path)
            reload_unit("nginx")
            sys.exit(5)

        # And we configure nginx to use the new TLS certs if they landed
        with open(config_path, "w") as f:
            template = env.get_template("nginx.conf.tpl")
            tvars = { "user": user, "domain": args.domain, "tls": True }
            f.write(template.render(tvars))
        reload_unit("nginx")

        print("Done!")
    else: # remove is set
        if not os.path.exists(config_path):
            print("Not configured to serve this domain.")
            sys.exit(5)
        with open(config_path, "r") as f:
            if f.read().find("# MAINT: owner: {}".format(user)) < 0:
                print("This domain does not appear to belong to you.")
                sys.exit(5)
        os.remove(config_path)
        reload_unit("nginx")
        print("Done!")


def init():
    "Register this tool with the argument parser"
    parser_web_desc = """
    This tool can be used to configure the waffle.tech web server to host an
    external domain name. You will need to add DNS configuration for that name
    before you run this tool so that we can verify ownership."""

    parser_web = TOOLS.add_parser("web", description=parser_web_desc, help="Manage external domain hosting")
    parser_web.add_argument("--remove", "-r", help="Remove domain", action="store_true")
    parser_web.add_argument("--no-www", "-w", help="Do not also configure for www prefix", action="store_true")
    parser_web.add_argument("domain", help="Domain name (without www)")
    parser_web.set_defaults(func=sub_web)


init()
