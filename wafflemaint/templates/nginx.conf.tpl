# Managed by maint
# MAINT: owner: {{ user }}

server {
  listen 204.152.206.250:80;
  listen [2607:fcd0:100:ae00::2]:80;
  server_name {{ domain }} www.{{ domain }};

  location ~ /.well-known {
    root /var/www/well-known;
  }

  {% if tls %}
    location / { return 301 https://$host$request_uri; }
  {% else %}
    root /home/{{ user }}/www;
  {% endif %}
}

{% if tls %}
server {
  listen 204.152.206.250:443;
  listen [2607:fcd0:100:ae00::2]:443;
  server_name {{ domain }} www.{{ domain }};

  ssl_protocols TLSv1.2;
  ssl_ciphers HIGH:!MEDIUM:!LOW:!aNULL:!NULL:!SHA;
  ssl_prefer_server_ciphers on;
  ssl_session_cache shared:SSL:10m;

  ssl_certificate     /etc/letsencrypt/live/{{ domain }}/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/{{ domain }}/privkey.pem;

  root /home/{{ user }}/www;

}
{% endif %}
